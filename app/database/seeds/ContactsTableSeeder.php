<?php

class ContactsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('contacts')->truncate();

		$contacts = array(
			['name' => 'Alejandro Sanabria', 'email' => 'alejandro@brandspa.com', 'company' => 'Brand Spa'],
			['name' => 'Daniel Hernandez', 'email' => 'daniel@brandspa.com', 'company' => 'Brand Spa']
		);

		// Uncomment the below to run the seeder
		//DB::table('contacts')->insert($contacts);
	}

}
