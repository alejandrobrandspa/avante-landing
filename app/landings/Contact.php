<?php namespace Landings;

use LaravelBook\Ardent\Ardent;

class Contact extends Ardent {

	public $autoHydrateEntityFromInput = true;
	
	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		'company' => 'required',
		'mail' => 'required|email',
		'phone' => 'required'
	);
}
