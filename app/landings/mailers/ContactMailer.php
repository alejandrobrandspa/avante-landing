<?php namespace Landings\Mailers;

class ContactMailer extends Mailer {

	public function notify($contact) 
	{
		$view = 'emails.contact';
		$data = [
			'promo' => $contact->promo,
			'name' => $contact->name,
			'company' => $contact->company,
			'mail' => $contact->mail,
			'phone' => $contact->phone,
			'created_at' => $contact->created_at
		];
		
		$to = 'alejandro@brandspa.com';
		$cc = ['daniel@brandspa.com', 'comercial@avante.cc'];
		$subject = 'Contacto landing promo '. $contact->promo;

		$this->send($view, $data, $to,$cc, $subject);
	}
}