<?php namespace Landings\Mailers;

use Mail;

abstract class Mailer {

	public function send($view, $data, $to, $cc=null, $subject) 
	{
		Mail::send($view, $data, function($message) use($to, $cc, $subject)
		{
			$message->to($to)->subject($subject);
			if ($cc) $message->cc($cc);
		});	
	}
}