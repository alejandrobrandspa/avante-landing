<?php namespace Landings\Repositories;

use Landings\Contact;

class ContactRepository {

	public function find($id) 
	{
		return Contact::find($id);
	}

	public function store() 
	{
		//new contact
		$contact = new Contact;
		//if contact created return object
		if($contact->save()) return $contact;
		//else return errors of validation
		return $contact->errors();
	}
}