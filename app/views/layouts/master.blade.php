<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	  <meta name="description" content="Descubra porqué en computadores y equipos tecnológicos es mejor negocio rentar que comprar">
	  <meta name="keywords" content="alquiler de tecnologia de punta para empresas, Alquiler de impresoras, printers, video bean, scanner, computadores de escritorio, Desktops, portatiles, Laptops, Apple, equipos Mac, Imac, Ipad, MacBook, redes, Network, servicio IT, IT service, Servidores, Service">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Avante</title>
		<!-- STYLES -->
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="[[ asset('vendor/bootstrap/dist/css/bootstrap.min.css') ]]">
	<link rel="stylesheet" href="[[ asset('vendor/alertify/themes/alertify.core.css') ]]">
	<link rel="stylesheet" href="[[ asset('vendor/alertify/themes/alertify.default.css') ]]">
	<link rel="stylesheet" href="[[ asset('css/theme-avante-a.css') ]]">
	<script src="[[ asset('js/modernizr.custom.js') ]]"></script>
	@section('custom_css')
	@show
</head>
<body>
	<div class="container">
		@yield('content')
	</div>

	<!-- libs -->
	<script src="[[ asset('vendor/jquery/jquery.min.js') ]]"></script>
	<script src="[[ asset('vendor/alertify/alertify.min.js') ]]"></script>
	<script src="[[ asset('js/jquery.fittext.js') ]]"></script>
	<script src="[[ asset('js/jquery.hoverdir.js') ]]"></script>
	<script type="text/javascript" src="[[ asset('vendor/bootstrap/dist/js/bootstrap.min.js') ]]"></script>			
	<!-- app -->
	<script src="[[ asset('js/app/contact.js') ]]"></script>
	<!-- scripts -->
	<script>
		jQuery("h1").fitText(1, {minFontSize: '16px', maxFontSize: '38px' });
		jQuery("h2").fitText(1, {minFontSize: '12px', maxFontSize: '28px' });
		jQuery("h3").fitText(1, {minFontSize: '10px', maxFontSize: '24px' });

		$(document).ready(function(){
			$(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );
		});

	</script>


<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
	$.src='//v2.zopim.com/?1Zc5JSoCKgNSnirkwSKyBcbL9PuAhpyE';z.t=+new Date;$.
	type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42534064-2', 'spinz.net');
  ga('send', 'pageview');

</script>

<script type="text/javascript">
$(document).on('ready', function(e){
	e.preventDefault();
	$('#legal').modal({show:false});
});
</script>
</body>
</html>