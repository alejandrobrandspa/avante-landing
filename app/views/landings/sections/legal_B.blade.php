<div class="modal fade" id="legal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Aplican Condiciones</h4>
      </div>
      <div class="modal-body">
        <p>
         Aplica exclusivamente para clientes nuevos en su primer pedido. Para recibir el beneficio se debe referenciar cómo se recibió la información. No acumulable con otras promociones. La impresora se incluye con el alquiler mínimo de un (1) equipo. Aplica con modelos por disponibilidad de inventario en referencias seleccionadas.  La impresora no incluye consumibles, se instala y se recoge al término del  alquiler del equipo, las perdidas o daños parciales o totales serán cubiertos por al cliente. No acumulable con otras promociones.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->