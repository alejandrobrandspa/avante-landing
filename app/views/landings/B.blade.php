@extends('layouts.master')
@section('custom_css')
@parent
	<style>
	body{
		background: url('../img/landing-avante2.jpg')  no-repeat #000;
	}

	.container-promo h1{
	font-size: 
	}

	
	</style>
@stop
@section('content')

<div class="row">
	<div class="col-lg-6">
		<div class="container-promo">
			<h1>
				Descubra porqué en
				<span class="yellow">computadores y equipos tecnológicos</span>
				es mejor negocio rentar que comprar.
			</h1>
			<div class="line"></div>
			<img src="[[ asset('img/logo.png') ]]" class="img-responsive">
		</div>
	</div>
	
	<div class="col-lg-6">
		<div class="container-form pull-left">
			<h2>RECIBA GRATIS <span class="yellow">UNA IMPRESORA</span><br> EN ALQUILER DURANTE UN MES CON SU PRIMERA ORDEN</h2>
			<h3>LLAME AHORA <span>PBX 6361051</span><br> 0 ENVÍE SUS DATOS</h3>
			<form id="form-b" class="form">
				<div class="form-group">
					<input type="text" name="name" class="form-control" placeholder="Nombre" >
				</div>
				<div class="form-group">
					<input type="text" name="company" class="form-control" placeholder="Empresa" >
				</div>
				<div class="form-group">
					<input type="text" name="mail" class="form-control" placeholder="Mail" >
				</div>
				<div class="form-group">
					<input type="text" name="phone" class="form-control" placeholder="Teléfono" >
				</div>
				<input type="hidden" name="promo" value="GRATIS UNA IMPRESORA">
				<a href="#" data-toggle="modal" data-target="#legal">Aplican Condiciones</a>
				<a href="#" class="btn btn-warning pull-right contact-store">Enviar</a>
			</form>
		</div>
	</div>
</div> <!-- end row -->

<p></p>

@include('landings.sections.products')
@include('landings.sections.legal_B')
@stop