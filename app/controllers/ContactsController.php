<?php

use Landings\Repositories\ContactRepository as Contact;
use Landings\Mailers\ContactMailer as Mailer;

class ContactsController extends BaseController {

	protected $contact;
	protected $mailer;
	
	public function __construct(Contact $contact, Mailer $mailer) 
	{
		$this->contact = $contact;
		$this->mailer = $mailer;
	}

	public function store()
	{
		$contact = $this->contact->store();
		
		if (isset($contact->id)) {
			$this->mailer->notify($contact);
		}
		return $contact;
	}

	public function show($id)
	{
        return View::make('contacts.show');
	}


	public function edit($id)
	{
        return View::make('contacts.edit');
	}


}
