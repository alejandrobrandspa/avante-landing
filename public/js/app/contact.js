window.Landings = {};

$(function(){

	Landings.Contact = {

		initialize: function(){
			$(document.body).on('click', '.contact-store', this.store);
		},
			
		store: function(evt) {

			evt.preventDefault();
			var $this = $(this);
			form = $('.form');
			data = $this.parent().serialize();

			workResponse = function(object){
				if (object.created_at) {
					form.fadeOut();
					$('.container-form h3').after('<h3><span class="yellow">Gracias</span>,<br> hemos recibido sus datos,<br> pronto nos estaremos contactando con usted.</h3>');
					$('.container-form h3').after('<iframe src="http://tracking.redrock-interactive.com/aff_l?offer_id=542" scrolling="no" frameborder="0" width="1" height="1"></iframe>');
					setTimeout(function(){
						window.location.href = "http://avante.cc";
					},5000);

				} else {
					$.each(object, function(key, val){
						alertify.error(val);
					});
				};
			};

			$.post('contacts', data).done(workResponse);
		}

	};

Landings.Contact.initialize();

});